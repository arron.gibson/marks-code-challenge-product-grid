# Product Grid Coding Task

## Overview

The Product Grid React App is a responsive web application designed to display a dynamic product grid sourced from a JSON feed. It utilizes React.js for building user interfaces and integrates with the Bootstrap CSS framework for a sleek and responsive design. The app fetches product data from the [dummyjson.com API](https://dummyjson.com/), allowing users to browse through a visually appealing grid of products. Each product card features essential details, such as title, brand, product image, original price, discounted price, and rating. The grid adapts its layout based on the screen size, showing four cards per row on extra-large screens, three on medium and large screens, and one on small screens.

Users can interact with the product grid through search functionality, category filtering, and sorting options by price or rating. The search input allows users to find specific products, while the category dropdown enables filtering by product category. Sorting options provide flexibility for users to arrange products based on price or rating in ascending or descending order. The application aims to provide an intuitive and visually appealing interface for users to explore and interact with product data efficiently.


## Task Requirements

Please review the [Project Requirements Documentation](./docs/project-requirements.md).

## Development

Please review the [Developer Documentation](./docs/development.md).

## Deployment

Simply update the `main` branch to deploy to GitLab pages at:

https://product-grid.arrongibson.com/
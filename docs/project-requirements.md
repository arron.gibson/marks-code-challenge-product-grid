# RESTful API Consumption and Product Grid Task

## Overview

This coding task involves consuming a public RESTful API to create a product grid that displays essential product details. The task requires building a responsive layout with features like pagination or lazy loading and sorting options for users.

## Task Requirements

1. **API Consumption:**
   - Utilize the provided public RESTful API to retrieve product data.

2. **Product Grid:**
   - Create a product grid to display the following details:
     - Title
     - Brand
     - Product Image
     - Original Price
     - Discounted Price
     - Rating

3. **Display Options:**
   - Implement a responsive layout for the product grid.
   - Provide options for pagination or lazy loading (choose one).

4. **Sorting:**
   - Allow users to sort products by rating, pricing, or categories.

5. **Technical Considerations:**
   - Optimize image display as necessary.
   - Address any concerns related to cross-site scripting attacks.

## Technical Details

- **Frameworks:**
  - You have the freedom to choose frameworks for this task. Consider using React for a competitive advantage.

- **Persistence:**
  - Similar to the first task, there is no requirement for server persistence.

- **Caching:**
  - You are not required to implement caching; hitting the API for each load is acceptable.

- **Submission:**
  - Provide a link to the code repository, ensuring it is accessible for code review.

## Bonus Features (Optional)

Feel free to include additional features such as:
- Displaying multiple images per product (with options like a slider).
- Additional filtering options (categories, etc.).
- Implementation of advanced display features.

## Data Details

- **API Endpoint:**
  - `https://dummyjson.com/products`

- **Total Products:**
  - 100

- **Skip and Limit Parameters:**
  - Skip: 0
  - Limit: 30

### API Docs

The documentation for the API is available here: https://dummyjson.com/docs

## Timeline

Please aim to complete this task by `2024.03.04`. We appreciate your effort and look forward to reviewing your submission. Best of luck!